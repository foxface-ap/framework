<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CdlNotesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CdlNotesTable Test Case
 */
class CdlNotesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CdlNotesTable
     */
    public $CdlNotes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cdl_notes',
        'app.authors'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CdlNotes') ? [] : ['className' => CdlNotesTable::class];
        $this->CdlNotes = TableRegistry::getTableLocator()->get('CdlNotes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CdlNotes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
