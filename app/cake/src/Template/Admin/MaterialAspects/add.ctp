<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\MaterialAspect $materialAspect
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($materialAspect) ?>
            <legend class="capital-heading"><?= __('Add Material Aspect') ?></legend>
            <?php
                echo $this->Form->control('material_aspect');
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Material Aspects'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts Materials'), ['controller' => 'ArtifactsMaterials', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifacts Material'), ['controller' => 'ArtifactsMaterials', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
